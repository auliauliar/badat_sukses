from django.apps import AppConfig


class AppBasdatSuksesConfig(AppConfig):
    name = 'app_basdat_sukses'
